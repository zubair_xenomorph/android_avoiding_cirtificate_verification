package com.evolint.androidble;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Zubair on 6/19/2016.
 */
public class Control extends AppCompatActivity{
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    private TextView dev_name;
    private TextView dev_addr;
    private TextView con_stat;
    private bleService bleServ;
    private String bleDeviceName;
    private String bleDeviceAddress;
    private IntentFilter intentFilter;
    private BluetoothGattCharacteristic characteristicTx;
    private BluetoothGattCharacteristic characteristicRx;
    private Button on;
    private Button off;
    private TextView data;
    public boolean isConnected = false;



    private final ServiceConnection bleServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            Log.d("zubair", "service connected");
            bleServ = ((bleService.LocalBinder) service).getService();
            if (!bleServ.initialize()) {
                Log.e("zubair", "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            bleServ.connect(bleDeviceAddress);

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bleServ = null;
        }
    };

    private final BroadcastReceiver bcReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d("zubair","broadcast received "+action);

            if(action.equals("androidBLE.connected")){
                Log.d("zubair","service discovered connected");
                con_stat.setText("connected");
                isConnected = true;
            }
            else if (action.equals("androidBLE.disconnected")) {
                Log.d("zubair","service discovered disconnected");
                con_stat.setText("not Connected");
                isConnected = false;
            }
            else if (action.equals("androidBLE.dataRead")) {
                String value = intent.getStringExtra("data");
                data.setText(intent.getStringExtra("data"));
                Log.d("zubair",value);
                //tsk.execute(url);
                if(value.equals("0"))
                {
                    Log.d("zubair","sending 0");
                    MyTask tsk = new MyTask();
                    tsk.execute("https://rumytechnologies.com/newtask/default/get_device_data?rumy_id=B0:B4:48:CF:6C:00&btn=0&amb_temp=12&target_temp=44&humidity=2");
                }
                else if(value.equals("1"))
                {
                    Log.d("zubair","sending 1");
                    MyTask tsk = new MyTask();
                    tsk.execute("https://rumytechnologies.com/newtask/default/get_device_data?rumy_id=B0:B4:48:CF:6C:00&btn=1&amb_temp=12&target_temp=44&humidity=2");
                }
                Log.d("zubair","data received"+intent.getStringExtra("data"));

            }
            else if (action.equals("androidBLE.serviceDiscovered"))  {
                Log.d("zubair", "service discovered broadcast received");
                getCharacterstics(bleServ.getGattServices());
            }

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.control_layout);
        intentFilter = new IntentFilter();
        intentFilter.addAction("androidBLE.connected");
        intentFilter.addAction("androidBLE.disconnected");
        intentFilter.addAction("androidBLE.serviceDiscovered");
        intentFilter.addAction("androidBLE.dataRead");


        on = ((Button)findViewById(R.id.btn_on));
        off = ((Button)findViewById(R.id.btn_off));

        data=(TextView)findViewById(R.id.r_enc);

        on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LedOn();
            }
        });
        off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LedOff();
            }
        });

    }

    @Override
    protected void onResume() {
        final Intent intent = getIntent();
        dev_name=(TextView)findViewById(R.id.dev_name_txt);
        dev_addr=(TextView)findViewById(R.id.dev_add_txt);
        con_stat=(TextView)findViewById(R.id.con_stat_txt);
        dev_name.setText(intent.getStringExtra(EXTRAS_DEVICE_NAME));
        dev_addr.setText(intent.getStringExtra(EXTRAS_DEVICE_ADDRESS));
        con_stat.setText("connecting..");
        bleDeviceName=intent.getStringExtra(EXTRAS_DEVICE_NAME);
        bleDeviceAddress=intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        registerReceiver(bcReceiver,intentFilter);
        Intent gattServiceIntent = new Intent(this, bleService.class);
        bindService(gattServiceIntent, bleServiceConnection, BIND_AUTO_CREATE);
        Log.d("zubair", "bound to the service in on resume");
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mInf = getMenuInflater();
        mInf.inflate(R.menu.control_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.discon)
        {
            bleServ.disconnect();
        }
        if(id == R.id.exit)
        {
            finish();
            System.exit(0);
        }
        return super.onOptionsItemSelected(item);
    }

    private void getCharacterstics(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        for (BluetoothGattService gattService : gattServices) {
            characteristicTx = gattService.getCharacteristic(bleService.UUID_HC_08_RX_TX);
            characteristicRx = gattService.getCharacteristic(bleService.UUID_HC_08_RX_TX);
        }
        Log.d("zubair","charactersticsRxTx = "+characteristicTx.toString());
        LedOff();

    }

    private void LedOn()
    {
        final byte[] tx = "1".getBytes();
        if(isConnected) {
            characteristicTx.setValue(tx);
            bleServ.writeCharacteristic(characteristicTx);
            bleServ.setCharacteristicNotification(characteristicRx, true);
        }
    }

    private void LedOff()
    {
        final byte[] tx = "0".getBytes();
        if(isConnected) {
            characteristicTx.setValue(tx);
            bleServ.writeCharacteristic(characteristicTx);
            bleServ.setCharacteristicNotification(characteristicRx,true);
        }
    }

    private class MyTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String content = HttpManager.send(params[0]);

            return content;
        }

        @Override
        protected void onPostExecute(String s) {

        }
    }
}
